# FPGA Exercises

The purpose of this repo is to centralized small and random projects.

FPGA Mastery

    Master, Slave
        Interfaces
            I2C
            SPI
            UART
            CAN
            SRAM
            DDR
        Busses, interconnect, mux, demux
            Wishbone
            AXI Lite
        Verification
            
        
       CDC

    
    

## Content

The projects belong to either one of the following categories:

* Interfaces
* Busses
* CDC

The idea is to make complete systems (master, slave, interconnect, crossbar switches) to gain experience.
All modules will be formally verified by providing:

* Formal definition
* Interface property set
* Coverage
* Induction

## Interfaces

### SPI

### I2C

### UART

### CAN?

## Busses

### Wishbone

### AXI Lite

### OBI

## CDC

## License

Free of use but without any warranty, probably useless for anyone else. But you never know.